# Minimal boilerplate for Webpack / Babel project

Current repo consists of several branches available to start your project with.

## Minimal boilerplate instructions

1. Install dependencies as per package.json

```
npm install
```

2. Edit src/main.js

3. Run npm script for compilation (includes webpack dev live reload server)

```
npm start
```

# OR

```
npm run build
```

## Minimal with webpack dev server | auto-reload | sass

1. Checkout branch `heavy`

```
git checkout heavy
```

2. Install dependencies

```
npm install
```

3. Run

```
npm start
```

4. Open in your browser

```
http://127.0.0.1:9001
```

## Minimal with Laravel mix | BrowserSync

1. Checkout branch `mix`

```
git checkout mix
```

2. Install dependencies

```
npm install
```

3. Run

```
npm run watch
```
4. Follow instructions in console

## Minimal with Laravel mix | BrowserSync | TypeScript

1. Checkout branch `mix`

```
git checkout mix-ts
```

2. Install dependencies

```
npm install
```

3. Run

```
npm run watch
```
4. Follow instructions in console